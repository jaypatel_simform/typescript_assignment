interface calculator {
    // all variables that need to declare in class
    dis: HTMLInputElement;
    memoryPart: HTMLTableElement;
    xCube: HTMLButtonElement;
    cubeRoot: HTMLButtonElement;
    yRootX: HTMLButtonElement;
    twoRaisedX: HTMLButtonElement;
    eRaisedX: HTMLButtonElement;
    logyX: HTMLButtonElement;
    value: string | number;
    memoryValue: number[];
    toggleValue: boolean;
    final_answer: string;

    // all functions that need to declare in class
    setExpresion(a: string): void;
    addPI(): void;
    getResult(): void;
    clear_display(): void;
    clear_last_digit(): void;
    factorial(): void;
    getRandomValue(): void;
    e_raised_to_x(): void;
    cube(): void;
    setFloorValue(a: string): void;
    changeFunction(): void;
    EF(): void;
    findln(): void;
    memoryFunction(id: string): void;
    mathFunction(func: string): void;
    updateExpression1(a: string): void;
    setValue(v: string): void;

}
class expressions implements calculator {

    dis = (<HTMLInputElement>document.getElementById('dis'));
    memoryPart = (<HTMLTableElement>document.getElementById('memory'));
    xCube = (<HTMLButtonElement>document.getElementById("x3"));
    cubeRoot = (<HTMLButtonElement>document.getElementById("cbrt"));
    yRootX = (<HTMLButtonElement>document.getElementById("yRootx"));
    twoRaisedX = (<HTMLButtonElement>document.getElementById("2x"));
    eRaisedX = (<HTMLButtonElement>document.getElementById("ex"));
    logyX = (<HTMLButtonElement>document.getElementById("logyx"));

    //displaythis.value
    value: string;
    //  iable for memorythis.value
    memoryValue: number[];
    //toggle button
    toggleValue: boolean;
    final_answer: string;

    constructor(value: string, memoryValue: number[], toggleValue: boolean, final_answer: string) {
        this.value = value
        this.memoryValue = memoryValue
        this.toggleValue = toggleValue
        this.final_answer = final_answer
    }

    setExpresion(a: string): void {
        if (this.value == "0") { this.value = ""; }
        this.value += a;
        this.dis.value = (this.value).toString();
    }

    addPI(): void {
        this.setExpresion(Math.PI.toFixed(6));
        this.dis.value = this.value;
    }

    //to get result of expression
    getResult(): void {
        // condition to for '| |' math operation
        if (this.value.includes("|-")) {
            this.value = this.value.slice(2, -1)
            this.final_answer = this.value;
        }
        //calculate expression
        else if (this.value) {
            this.final_answer = eval(this.value);
        }
        else
            this.final_answer = "0";
        //setthis.value( iable) with final answer
        this.value = this.final_answer.toString();
        // display final answer
        this.dis.value = this.value
    }

    // function to clear screen
    clear_display(): void {
        this.value = "0";
        this.setExpresion(this.value);
    }
    // function to clear last digit
    clear_last_digit(): void {
        this.value = this.value.slice(0, this.value.length - 1);
        if (!this.value.length) { this.value = "0"; }
        this.dis.value = this.value;
    }

    // for calculate the factorial
    factorial(): void {
        let number = Number(this.value);
        let factofNumber = 1;
        for (let i = 2; i <= number; i++)
            factofNumber = factofNumber * i;
        this.value = factofNumber.toString();
        this.dis.value = this.value;
    }

    // for generate randomthis.value
    getRandomValue(): void {
        let randValue: number = Math.random();
        if (this.value) {
            this.setExpresion(randValue.toString());
        }
        else {
            this.value = randValue.toString();
            this.dis.value = this.value;
        }
    }

    // for calculate e Raised to xthis.value
    e_raised_to_x(): void {
        let lastDigit = this.value.slice(-1);
        this.final_answer = Math.pow(2.7182, Number(lastDigit)).toString();
        this.value = this.value.slice(0, -1);
        this.setExpresion(this.final_answer);
    }

    // for calculate cubethis.value
    cube(): void {
        let lastDigit = this.value.slice(-1);
        this.final_answer = Math.pow(Number(lastDigit), 3).toString();
        this.value = this.value.slice(0, -1);
        this.setExpresion(this.final_answer);
    }

    // for set floorthis.value
    setFloorValue(a: string): void {
        // condition to set floor down
        if (a == "fd")
            this.value = Math.floor(Number(this.value)).toString();
        // condition to set floor up
        else
            this.value = parseInt(this.value + 1).toString();
        // set display content
        this.dis.value = this.value;
    }

    // for button that change some math function with some new functions
    changeFunction(): void {

        if (this.toggleValue) {
            this.xCube.innerText = "x3";
            this.xCube.value = "x3";

            this.cubeRoot.innerText = "3√x";
            this.cubeRoot.value = "Math.cbrt(";

            this.yRootX.innerText = "y√x";
            this.yRootX.value = "Math.pow(";

            this.twoRaisedX.innerText = "2^x";
            this.twoRaisedX.value = "Math.pow(2,";

            this.logyX.innerText = "logyX";

            this.eRaisedX.innerText = "eX";
            this.eRaisedX.value = "ex";
            this.toggleValue = !this.toggleValue;
        }
        else {
            this.xCube.innerText = "x²";
            this.xCube.value = "Math.pow(,2)";

            this.cubeRoot.innerText = '2√x';
            this.cubeRoot.value = "Math.sqrt(";

            this.yRootX.innerText = 'x^y';
            this.yRootX.value = "Math.pow(";

            this.twoRaisedX.innerText = '10^x';
            this.twoRaisedX.value = "Math.pow(10,";

            this.logyX.innerText = "log";

            this.eRaisedX.innerText = "ln";
            this.eRaisedX.value = "ln";
            this.toggleValue = !this.toggleValue;
        }
    }

    // set diaplaythis.valuein exponent form
    EF(): void {
        let v: number = Number(this.value);
        let ans: string = v.toExponential();
        this.value = "0";
        this.setExpresion(ans);
    }
    findln(): void {
        let number = Number(this.value);
        this.final_answer = (Math.log(number) / Math.log(2.71828)).toString();
        this.value = this.final_answer
        this.dis.value = this.value;
    }

    // calculate all memory functions
    memoryFunction(id: string): void {
        switch (id) {
            // to save display answer in memory
            case "MS":
                this.memoryValue.unshift(Number(this.value));
                break;
            // to clear allthis.valuein memory
            case "MC":
                this.memoryValue = [];
                break;
            // to add display vlaue with memorythis.value
            case "M+":
                if (this.memoryValue[0])
                    this.memoryValue[0] += Number(this.value);
                break;
            // to subtract display vlaue with memorythis.value
            case "M-":
                if (this.memoryValue[0])
                    this.memoryValue[0] -= Number(this.value);
                break;
            // to recall latest vlaue in memory
            case "MR":
                if (this.memoryValue[0]) {
                    let lastValue = this.memoryValue[0];
                    this.setExpresion(lastValue.toString());
                }
                break;
        }
        // to add reslut in memory vlaue
        this.memoryPart.innerHTML = this.memoryValue.toString();
    }

    // to calculate trignometry functions
    mathFunction(func: string): void {
        // get the degree from the user
        let degrees = Number(this.value.slice(-2));
        this.value = this.value.slice(0, this.value.length - 2);
        // convert degree into radius
        let radians = (degrees * Math.PI) / 180;
        let final_answer = 0;
        // calculate final answer according user given function
        switch (func) {
            case "sin":
                final_answer = Math.sin(radians);
                break;
            case "cos":
                final_answer = Math.cos(radians);
                break;
            case "tan":
                final_answer = Math.tan(radians);
                break;
            case "cosec":
                final_answer = 1 / Math.sin(radians);
                break;
            case "sec":
                final_answer = 1 / Math.cos(radians);
                break;
            case "cot":
                final_answer = 1 / Math.tan(radians);
                break;
        }
        // set expression vlaue with final answer
        this.setExpresion(final_answer.toFixed(2));
    }

    updateExpression1(a: string): void {
        switch (a) {
            case "Math.PI":
                this.addPI();
                break;
            case "=":
                this.getResult();
                break;
            case "clear":
                this.clear_display();
                break;
            case "CL":
                this.clear_last_digit();
                break;
            case "!":
                this.factorial();
                break;
            case "randValue":
                this.getRandomValue();
                break;
            case "ex":
                this.e_raised_to_x();
                break;
            case "x3":
                this.cube();
                break;
            case "fu":
                this.setFloorValue(a);
                break;
            case "fd":
                this.setFloorValue(a);
                break;
            case "ln":
                this.findln();
                break;
            default:
                this.setExpresion(a);
        }
    }
    setValue(v: string): void {
        this.value = v;
    }

}

//declare class object below
const exp = new expressions("0", [], false, "")

function updateExpresion(a: string) {
    exp.updateExpression1(a);
}

// to update funcitons
function updateFunction() {
    exp.changeFunction();
}

// to manipulate memorythis.value
function memoryFunction1(id: string) {
    exp.memoryFunction(id);
}
// to calculate trignometry functions
function calculateTrigo(func: string) {
    exp.mathFunction(func);
}
// to set displaythis.valueinto exponential form
function setEF() {
    exp.EF();
}

// if you want to add something between expression
function inputBetweenExp() {
    const displayValue = (<HTMLInputElement>document.getElementById('dis')).value;
    exp.setValue(displayValue);
}
